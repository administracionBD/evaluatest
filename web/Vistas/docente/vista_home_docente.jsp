<%-- 
    Document   : vista_home_docente
    Created on : 28/10/2018, 10:54:36 PM
    Author     : sebastian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home Administrador</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
        <link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link href="framework/css/app.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    </head>
    <body> 
        <div style="background-image: url('../../framework/img/fondo.jpg'); width: 100; height: 100;">
            <nav class="top-bar" data-topbar>
                <ul class="title-area">

                    <li class="name">
                        <h1>
                            <a href="Vista_home_Administrador.jsp">
                                Docente
                            </a>
                        </h1>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                </ul>
                <section class="top-bar-section">

                    <ul class="right">
                        <li class="divider"></li>
                        <li class="has-dropdown">
                            <a href="#">Acciones</a>
                            <ul class="dropdown">
                                <li><label>Estructura del examen</label></li>
                                <li class="has-dropdown">
                                    <a href="#" class="">Gestionar preguntas</a>
                                    <ul class="dropdown">
                                        <li><a href="sobreTema/tema.jsp">Preguntas</a></li>
                                    </ul>
                                </li>
                                <li class="has-dropdown">
                                    <a href="#" class="">Gestionar examenes</a>
                                    <ul class="dropdown">
                                        <li><a href="sobreGrupo/cursos.jsp">Examenes</a></li>
                                    </ul>
                                </li>
                                <li class="divider"></li>
                                <li><a href="./vistaGestionDocente.jsp">Mi perfil</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../index.jsp">Cerrar sesion</a></li>
                    </ul>
                </section>
            </nav>


            <div class="row">
                <div class="large-12 columns">
                    <center>
                        <br>
                        <img  src="../../framework/img/imagenBienvenida.jpg" ><br><br>

                    </center>


                </div>
            </div>

            <div class="row">
                <div class="large-3 panel columns">
                    <img src="../../framework/img/docente.jpg">
                    <h4>Docente</h4>
                    <p align="justify">Modulo de gestion de examenes y preguntas realizadas por el docente e informacion del perfil</p><hr>
                    <div class="row">
                        <div class="large-4 columns">
                            <a href="#" class="tiny button">Link</a>
                        </div>
                        <div class="large-4 columns">
                            <a href="#" class="tiny button">Link</a>
                        </div>
                        <div class="large-4 large-collapse columns">
                            <a href="#" class="tiny button">Link</a>
                        </div>
                    </div>
                </div>
                <div class="large-9 columns">
                    <h4 align="center">Mis examenes</h4>
                    <div class="panel">   
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="row">
            <div class="large-12 columns"><hr />
                <div class="row">
                    <div class="large-6 columns">
                        <p>Sistema de evaluaciones Evaluatest</p>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>

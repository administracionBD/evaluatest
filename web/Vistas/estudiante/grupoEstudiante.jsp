
<%-- 
    Document   : Vista_home_Administrador
    Created on : 20/10/2018, 06:41:04 PM
    Author     : Cristian
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home Estudiante</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
        <link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link href="framework/css/app.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    </head>
    <body>
        <div style="background-image: url('../../framework/img/fondo.jpg'); width: 100; height: 100;">
            <nav class="top-bar" data-topbar>
                <ul class="title-area">

                    <li class="name">
                        <h1>
                            <a href="vista_home_Estudiante.jsp">
                                Estudiante 
                            </a>
                        </h1>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                </ul>
                <section class="top-bar-section">

                    <ul class="right">
                        <li class="divider"></li>
                        <li class="has-dropdown">
                            <a href="#">Acciones</a>
                            <ul class="dropdown">


                                <li class="divider"></li>
                                <li><label>Mi informacion</label></li>
                                <li><a href="./perfil_Estudiante.jsp">Mi perfil</a></li>
                                <li><a href="./grupoEstudiante.jsp">Mis grupos</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../../index.jsp">Cerrar Sesion</a></li>
                        <li class="divider"></li>

                    </ul>
                </section>
            </nav>


            <div class="row">
                <div class="large-12 columns">
                    <center>
                        <img  src="../../framework/img/imagenBienvenida.jpg" ><br><br>

                    </center>


                </div>
            </div>

            <div class="row">
                <div class="large-3 panel columns">
                    <img src="../../framework/img/usuario.jpg">
                    <h4>Estudiante</h4>
                    <p>Usuario de Ingenieria de software</p><hr>
                    <div class="row">
                        <div class="large-4 columns">

                        </div> 

                    </div>
                </div>
                <div class="large-9 columns">
                    <div class="panel">
                        <div class="row">

                            <center>
                                <h2 >Grupos asociados</h2>
                                <table>
                                    <thead>
                                        <tr>
                                            <th width="200">GrupoID</th>
                                            <th>Nombre</th>
                                            <th width="150">Descripcion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>123</td>
                                            <td>Administracion</td>
                                            <td>Grupo que vera la administracion de base de datos</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                            <a href="vista_home_Estudiante.jsp" class="small button">Volver</a>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
        <footer class="row">
            <div class="large-12 columns"><hr />
                <div class="row">
                    <div class="large-6 columns">
                        <p>&copy; Copyright no one at all. Go to town.</p>
                    </div>

                </div>
            </div>
        </footer>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>

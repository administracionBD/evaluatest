
<%-- 
    Document   : Vista_home_Administrador
    Created on : 20/10/2018, 06:41:04 PM
    Author     : Cristian
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home Estudiante</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
        <link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>
        <link href="framework/css/app.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    </head>
    <body>
        <div style="background-image: url('../../framework/img/fondo.jpg'); width: 100; height: 100;">
            <nav class="top-bar" data-topbar>
                <ul class="title-area">

                    <li class="name">
                        <h1>
                            <a href="vista_home_Estudiante.jsp">
                                Estudiante 
                            </a>
                        </h1>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                </ul>
                <section class="top-bar-section">

                    <ul class="right">
                        <li class="divider"></li>
                        <li class="has-dropdown">
                            <a href="#">Acciones</a>
                            <ul class="dropdown">


                                <li class="divider"></li>
                                <li><label>Mi informacion</label></li>
                                <li><a href="./perfil_Estudiante.jsp">Mi perfil</a></li>
                                <li><a href="./grupoEstudiante.jsp">Mis grupos</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../../index.jsp">Cerrar Sesion</a></li>
                        <li class="divider"></li>

                    </ul>
                </section>
            </nav>


            <div class="row">
                <div class="large-12 columns">
                    <center>
                        <img  src="../../framework/img/imagenBienvenida.jpg"><br><br>
                    </center>


                </div>
            </div>

            <div class="row">
                <div class="large-3 panel columns">
                    <img src="../../framework/img/usuario.jpg">
                    <h4>Estudiante</h4>
                    <p>Usuario de Ingenieria de software</p><hr>
                    <div class="row">
                        <div class="large-4 columns">

                        </div> 

                    </div>
                </div>
                <div class="large-9 columns">
                    <div class="panel">
                        <div class="row">

                            <center>
                                <h2 >Examenes pendientes</h2>
                                <table>
                                    <thead>
                                        <tr>
                                            <th width="200">Examenes</th>
                                            <th>Docente</th>
                                            <th width="150">Fecha limite</th>
                                            <th width="150">Aciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Examen Base de datos</td>
                                            <td>Jorge ivan Triviño </td>
                                            <td>25/11/2018</td>
                                            <td><a href="examenEstudiante.jsp" class="small button">Presentar</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>

                        </div>
                    </div>
                    <div class="panel">
                        <div class="row">

                            <center>
                                <h2 >Resultados examenes</h2>
                                <table>
                                    <thead>
                                        <tr>
                                            <th width="200">Examen</th>
                                            <th>Docente</th>
                                            <th width="150">Fecha de presentacion</th>
                                            <th width="150">Fecha limite</th>
                                            <th width="150">Respuestas correctas</th>
                                            <th width="150">Resultado</th>
                                            <th width="150">Retroalimentacion</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Examen tablespace</td>
                                            <td>Alvaro </td>
                                            <td>24/11/2018</td>
                                            <td>25/11/2018</td>
                                            <td>2</td>
                                            <td>1.5</td>
                                            <td><a href="ResultadosExamen.jsp" class="small button">Ver</a></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </center>

                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
        <footer class="row">
            <div class="large-12 columns"><hr />
                <div class="row">
                    <div class="large-6 columns">
                        <p>&copy; Copyright no one at all. Go to town.</p>
                    </div>

                </div>
            </div>
        </footer>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>


<%-- 
    Document   : tema
    Created on : 20/10/2018, 08:45:53 PM
    Author     : Cristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Cursos Administrador</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
        <link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>


        <link href="../../framework/css/app.css" rel="stylesheet" type="text/css"/>

        <link href="framework/css/app.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    </head>
    <body class="body"  onload="ini()">
        <div style="background-image: url('../../framework/img/fondo.jpg'); width: 100; height: 100;">
            <nav class="top-bar" data-topbar>
                <ul class="title-area">

                    <li class="name">
                        <h1>
                            <a href="vista_home_Estudiante.jsp">
                                Estudiante
                            </a>
                        </h1>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                </ul>
                <section class="top-bar-section">

                    <ul class="right">
                        <li class="divider"></li>
                        <li class="has-dropdown">
                            <a href="#">Acciones</a>
                            <ul class="dropdown">


                                <li class="divider"></li>
                                <li><label>Mi informacion</label></li>
                                <li><a href="./perfil_Estudiante.jsp">Mi perfil</a></li>
                                <li><a href="./grupoEstudiante.jsp">Mis grupos</a></li>
                                <li class="divider"></li>
                            </ul>
                        </li>


                        <li class="divider"></li>
                        <li><a href="../../index.jsp">Cerrar Sesion</a></li>
                        <li class="divider"></li>


                    </ul>
                </section>
            </nav>
            <br/>
            <div class="row">
                <div class="large-12 columns">
                    <center>
                        <img  src="../../framework/img/examenes.png" ><br><br>
                    </center>


                </div>
            </div>

            <div class="row">
                <div class="large-3 panel columns">
                    <img src="../../framework/img/examenesDos.jpg">
                    <div class="row">
                        <div class="large-4 columns">
                            <a href="vista_home_Estudiante.jsp" class="tiny button">Volver</a>
                        </div>
                    </div>
                </div>
                <div class="large-9 columns">
                    <div class="panel">
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="small-12">
                                    <h4 class="text-center">Examen Base de datos</h4>
                                    <div class="">
                                        <div id="contador"></div>
                                        <label id="contador">12/11/2018
                                        </label>
                                        <label class="">¿Como se calcula el tamaño de un tablespace?
                                            <textarea rows="4" cols="50" name="txtAreaDescripcionCursos" placeholder="Ingrese una descripcion de la unidad"></textarea>
                                        </label>
                                        <fieldset class="large-7 cell">
                                            <legend>Marque solo las correctas</legend>
                                            <input id="checkbox1" type="checkbox" >Una tabla se representa fisicamente por el datafile <br></br><label for="checkbox1"></label>
                                            <input id="checkbox2" type="checkbox">Datafile es a esquema lo que clase es a proyecto de eclipse<br></br><label for="checkbox2"></label>
                                            <input id="checkbox3" type="checkbox">Es recomendable siemore tener al menos dos tablespace<br></br><label for="checkbox3"></label>
                                        </fieldset>
                                        <fieldset class="large-5 cell">
                                            <legend>Maria es administradora del sistema de informacion estudiantil, en el cula solo se pueden registrar y modificar las notas de los estudiantes los primeros 15 dias de cada mes, pero si puede consultarlos en cualquier mommento. Que cree que deberia hacer?</legend>
                                            <input type="radio" name="pokemon" value="Red" id="pokemonRed" required>A.<label for="pokemonRed">Crear un tablespace y mover alli las tablas de registro de notas para no afectar las demas tablas</label>
                                            <input type="radio" name="pokemon" value="Blue" id="pokemonBlue">B.<label for="pokemonBlue">Poner los index de la tabla notas en offline durante ese periodo de tiempo</label>
                                            <input type="radio" name="pokemon" value="Yellow" id="pokemonYellow">C.<label for="pokemonYellow">Analizar el sistema y mover todas las tablas estadisticas a un tablespace y las dinamicas a otro</label>
                                        </fieldset>
                                        <div class="large-6 columns">
                                            <div class="small-6">
                                                <label class="">Preguntar de enlazar</label>
                                                <label class="">A. Amarillo</label>
                                                <label class="">B. Rojo</label>
                                                <label class="">C. Azul</label>
                                            </div>
                                        </div>
                                        <div class="large-6 columns">
                                            <div class="small-6">
                                                <label class="">Seleccione la respuesta de cada una

                                                </label>
                                                <label class="">Yellow
                                                    <select name="cbTipoCurso" id="tema">
                                                        <option value="curso" >A</option>
                                                        <option value="curso">B</option>
                                                        <option value="curso">C</option>
                                                    </select>
                                                </label>
                                                <label class="">Blue
                                                    <select name="cbTipoCurso" id="tema">
                                                        <option value="curso">A</option>
                                                        <option value="curso">B</option>
                                                        <option value="curso">C</option>
                                                    </select>
                                                </label>
                                                <label class="">Red
                                                    <select name="cbTipoCurso" id="tema">
                                                        <option value="curso">A</option>
                                                        <option value="curso">B</option>
                                                        <option value="curso">C</option>
                                                    </select>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="vista_home_Estudiante.jsp" class="small button">Enviar</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <footer class="row">
            <div class="large-12 columns"><hr />
                <div class="row">
                    <div class="large-6 columns">
                        <p>&copy; Copyright https://foundation.zurb.com/templates-f5.html </p>
                    </div>
                    <div class="large-6 columns">
                        <p>Acerca de nosotros: Evaluatest</p>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js"></script>
        <script>
    $(document).foundation();
        </script>
        <script>
            var contador = 0;
            var fin_contador = 200; // Tiempo en en el que deseas que redireccione la funcion.
            var iniciado = false;
            function cuenta() {


                if (contador >= fin_contador) {
                    window.location.href = "./vista_home_Estudiante.jsp";
                } else {


                    document.getElementById("contador").innerHTML = "El examen termina en " + fin_contador + " Seg";

                    fin_contador = fin_contador - 1;
                }


            }

            function ini() {

                cuenta();
                setInterval("cuenta()", 1000);


            }
        </script>
    </body>
</html>
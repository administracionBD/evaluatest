<%-- 
    Document   : vistaGestionDocente
    Created on : 28/10/2018, 04:30:28 PM
    Author     : sebastian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../framework/css/foundation.css" rel="stylesheet" type="text/css"/>
        <link href="../framework/css/app.css" rel="stylesheet" type="text/css"/>
        <title>GESTION DOCENTES</title>
    </head>
    <body>
        <div class="top-bar">
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <h4><li class="menu-text">Evaluatest</li></h4>
                </ul>
            </div>
            <div class="top-bar-right">
                <ul class="menu">
                    <li><button onclick="location.href='./../index.jsp'" type="button" class="button">Iniciar Sesion</button></li>
                </ul>
            </div>
        </div>
        <div class="fondoRegEstu">
            <form>
                <div class="grid-container">
                    <h4 align="center"><b>Gestionar Informacion del Docente</b></h4>
                    <div class="grid-x grid-padding-x">
                        <div class="medium-6 cell">
                            <label class="color">Codigo Docente
                                <input type="text" name="txtCodigoDoc" placeholder="Ingresar el codigo">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Contraseña Docente
                                <input type="text" name="txtContrasenaDoc" placeholder="Ingresar una contraseña">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Documento Docente
                                <input type="text" name="txtDocuDoc" placeholder="Ingresar el # del documento">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Nombre Docente
                                <input type="text" name="txtNombreDoc" placeholder="Ingresar el nombre">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Apellido Docente
                                <input type="text" name="txtApellidoDoc" placeholder="Ingresar el apellido">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Fecha nacimiento
                                <input type="date" name="jdFechaNacDoc" placeholder="Escoja una fecha de nacimiento">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Telefono Docente
                                <input type="text" name="txtTelefonoDoc" placeholder="Ingresar el # de telefono">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Dirección Docente
                                <input type="text" name="txtDireccionDoc" placeholder="Ingresar la dirección">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Edad Docente
                                <input type="text" name="txtEdadDoc" placeholder="Ingresar la edad">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Seleccione un tipo de documento
                                <select name="cbTipoDoc">
                                    <option value="seleccion">Seleccione una opción</option>
                                    <option value="Cedula">Cedula</option>
                                    <option value="Tarjeta i.d">Tarjeta i.d</option>
                                    <option value="Pasaporte">Pasaporte</option>
                                </select>
                            </label>
                        </div>
                        <div class="medium-12 cell">
                            <button onclick="location.href='./../index.jsp'" type="button" class="primary button expanded search-button">
                                Registrarse
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="grid-x grid-y">    
            <div class="pie">
                <h4 class="pagination"><b>Sistema de evaluaciones</b></h4>
            </div>
        </div>
        <script src="../framework/js/vendor/foundation.js" type="text/javascript"></script>
        <script src="../framework/js/vendor/jquery.js" type="text/javascript"></script>
        <script src="../framework/js/vendor/what-input.js" type="text/javascript"></script>
        <script src="../framework/js/app.js" type="text/javascript"></script>
    </body>
</html>


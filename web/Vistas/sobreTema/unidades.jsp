<%-- 
    Document   : tema
    Created on : 20/10/2018, 08:45:53 PM
    Author     : Cristian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Contenido Administrador</title>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/normalize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/css/foundation.min.css">
        <link href='https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

        <link href="../../framework/css/app.css" rel="stylesheet" type="text/css"/>

        <link href="framework/css/app.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    </head>
    <body class="body">
        <div style="background-image: url('../../framework/img/fondo.jpg'); width: 100; height: 100;">
            <nav class="top-bar" data-topbar>
                <ul class="title-area">

                    <li class="name">
                        <h1>
                            <a href="../Vista_home_Administrador.jsp">
                                Administrador
                            </a>
                        </h1>
                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>menu</span></a></li>
                </ul>
                <section class="top-bar-section">

                    <ul class="right">
                        <li class="divider"></li>
                        <li class="has-dropdown">
                            <a href="#">Acciones</a>
                            <ul class="dropdown">
                                <li><label>Administrar Informacion</label></li>
                                <li class="has-dropdown">
                                    <a href="#">Temas y unidades</a>
                                    <ul class="dropdown">
                                        <li><a href="../sobreTema/tema.jsp">Temas</a></li>
                                        <li><a href="../sobreTema/unidades.jsp">Unidades</a></li>
                                    </ul>
                                </li>
                                <li class="has-dropdown">
                                    <a href="#" class="">Grupos y cursos</a>
                                    <ul class="dropdown">
                                        <li><a href="./../sobreGrupo/cursos.jsp">Cursos</a></li>
                                        <li><a href="./../sobreGrupo//grupos.jsp">Grupos</a></li>
                                    </ul>
                                </li>
                                <li class="divider"></li>
                                <li><label>Administrar Usuarios</label></li>
                                <li><a href="../vistaGestionDocente.jsp">Docentes</a></li>
                                <li><a href="../vistaGestionEstudiante.jsp">Estudiantes</a></li>
                            </ul>
                        </li>
                        <li class="divider"></li>
                        <li><a href="../../index.jsp">Cerrar sesion</a></li>
                    </ul>
                </section>
            </nav>
            <div class="row">
                <div class="large-12 columns">
                    <center>
                        <br>
                        <img  src="../../framework/img/unidades.jpg" ><br><br>
                    </center>


                </div>
            </div>

            <div class="row">
                <div class="large-3 panel columns">
                    <img src="../../framework/img/unidades2jpg.jpg">    
                </div>
                <div class="large-9 columns">
                    <div class="panel">
                        <div class="row">
                            <div class="large-12 columns">
                                <div class="small-12">
                                    <h4 class="text-center">Administracion de unidades</h4>
                                    <div class="">
                                        <label class="">Nombre unidades
                                            <input type="text" name="txtNombreunidades" placeholder="Ingrese el nombre de la unidad" required="true">
                                        </label>
                                        <label class="">Descripcion unidad
                                            <textarea rows="4" cols="50" name="txtAreaDescripcionUnidad" placeholder="Ingrese una descripcion de la unidad"></textarea>
                                        </label>
                                        <label class="">Seleccione un Curso
                                            <select name="cbTipoCurso" id="tema">
                                                <option value="curso">Seleccione un curso</option>
                                            </select>
                                        </label>
                                    </div>
                                    <a href="#" class="small button">Registrar</a>
                                    <a href="#" class="small button">Modificar</a>
                                    <a href="#" class="small button">Eliminar</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <div class="panel">
                                <label class="color">Nombre unidad
                                    <input type="text" name="txtNombreunidad" placeholder="Ingrese el nombre de la unidad" required="true">
                                </label>
                                <a href="#" class="small button">Buscar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="row">
            <div class="large-12 columns"><hr />
                <div class="row">
                    <div class="large-6 columns">
                        <p>Acerca de nosotros: Evaluatest</p>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>

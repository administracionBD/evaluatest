<%-- 
    Document   : vistaGestionEstudiante
    Created on : 20/10/2018, 06:21:56 PM
    Author     : sebastian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../framework/css/foundation.css" rel="stylesheet" type="text/css"/>
        <link href="../framework/css/app.css" rel="stylesheet" type="text/css"/>
        <title>GESTION ESTUDIANTES</title>
    </head>
    <body>
        <div class="top-bar">
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <h4><li class="menu-text">Evaluatest</li></h4>
                </ul>
            </div>
            <div class="top-bar-right">
                <ul class="menu">
                    <li><button onclick="location.href='./../index.jsp'" type="button" class="button">Iniciar Sesion</button></li>
                </ul>
            </div>
        </div>
        <div class="fondoRegEstu">
            <form>
                <div class="grid-container">
                    <h4 align="center"><b>Gestionar Informacion del Estudiante</b></h4>
                    <div class="grid-x grid-padding-x">
                        <div class="medium-6 cell">
                            <label class="color">Codigo Estudiante
                                <input type="text" name="txtCodigoEst" placeholder="Ingresar el codigo">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Contraseña Estudiante
                                <input type="text" name="txtContrasenaEst" placeholder="Ingresar una contraseña">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Documento Estudiante
                                <input type="text" name="txtDocEst" placeholder="Ingresar el # del documento">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Nombre Estudiante
                                <input type="text" name="txtNombreEst" placeholder="Ingresar el nombre">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Apellido Estudiante
                                <input type="text" name="txtApellidoEst" placeholder="Ingresar el apellido">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Fecha nacimiento
                                <input type="date" name="jdFechaNacEst" placeholder="Escoja una fecha de nacimiento">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Telefono Estudiante
                                <input type="text" name="txtTelefonoEst" placeholder="Ingresar el # de telefono">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Dirección Estudiante
                                <input type="text" name="txtDireccionEst" placeholder="Ingresar la dirección">
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Seleccione un tipo de documento
                                <select name="cbTipoDoc">
                                    <option value="seleccion">Seleccione una opción</option>
                                    <option value="Cedula">Cedula</option>
                                    <option value="Tarjeta i.d">Tarjeta i.d</option>
                                    <option value="Pasaporte">Pasaporte</option>
                                </select>
                            </label>
                        </div>
                        <div class="medium-6 cell">
                            <label class="color">Seleccione un Grupo
                                <select name="cbGrupo">
                                    <option value="seleccion">Seleccione una opción</option>
                                    //cargar los grupos desde la base de datos
                                </select>
                            </label>
                        </div>
                        <div class="medium-12 cell">
                            <button onclick="location.href='./../index.jsp'" type="button" class="primary button expanded search-button ">
                                Registrarse
                            </button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="grid-x grid-y">    
            <div class="pie">
                <h4 class="pagination"><b>Sistema de evaluaciones</b></h4>
            </div>
        </div>
        <script src="../framework/js/vendor/foundation.js" type="text/javascript"></script>
        <script src="../framework/js/vendor/jquery.js" type="text/javascript"></script>
        <script src="../framework/js/vendor/what-input.js" type="text/javascript"></script>
        <script src="../framework/js/app.js" type="text/javascript"></script>
    </body>
</html>
